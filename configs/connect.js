const connectSetting = {
    'metadata.broker.list':`${process.env.BROKER_LIST}`,
    'retry.backoff.ms': 200,
    // 'client.id': 'kafka',
    // 'compression.codec': 'gzip',
    'message.send.max.retries': 10,
    'socket.keepalive.enable': true,
    'queue.buffering.max.messages': 100000,
    'queue.buffering.max.ms': 1000,
    'batch.num.messages': 1000000,
    'dr_msg_cb': true,
    'event_cb': true,
    'request.required.acks': 1,
    'debug': 'broker,topic,msg',
    'fetch.wait.max.ms': 10000,
}

module.exports = {
    connectSetting
}