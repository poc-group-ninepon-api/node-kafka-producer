const Kafka = require('node-rdkafka')
const eventType = require('./eventType')
require('dotenv').config()
const express = require('express')
const app = express()
var randomstring = require("randomstring");
const waitConsumerResponse = false

const { connectSetting } = require('./configs/connect')

const stream = Kafka.Producer.createWriteStream(connectSetting, {} ,{topic: `${process.env.TOPIC}`})

function queueMessage(){
  const category = _getRandomAnimal();
  const noise = _getRandomNoise(category);
  const event = { category, noise ,"datetime":(new Date()).toISOString() };
  // console.log(event)
  
  const success = stream.write(eventType.toBuffer(event));//eventType.toBuffer(event) //Buffer.from(event)
  if(success){
    console.log(`message queued (${JSON.stringify(event)})`);
  }else{
    console.log('Too many messages in the queue already..');
  }

  stream.on('error', function (err) {
    // Here's where we'll know if something went wrong sending to Kafka
    console.error('Error in our kafka stream');
    console.error(err);
  })
}

function _getRandomAnimal() {
  const categories = ['CAT', 'DOG'];
  return categories[Math.floor(Math.random() * categories.length)];
}
  
function _getRandomNoise(animal) {
  if (animal === 'CAT') {
    const noises = ['meow', 'purr'];
    return noises[Math.floor(Math.random() * noises.length)];
  } else if (animal === 'DOG') {
    const noises = ['bark', 'woof'];
    return noises[Math.floor(Math.random() * noises.length)];
  } else {
    return 'silence..';
  }
}

setInterval(() => {
  // queueMessage();
}, 3000);

// ==============================================================================
// Response from conumer
let consumer = null
let responseConsumer = null
if(waitConsumerResponse ){
  consumer = Kafka.KafkaConsumer({
    'group.id': `${uniqueue}`,
    'metadata.broker.list':`${process.env.BROKER_LIST}`,
    'partition.assignment.strategy': 'range',
    'enable.auto.commit': true,
    'event_cb': true,
  })
  consumer.connect();
}

const uniqueue = "ninepon123"
var producer = Kafka.Producer({
  'metadata.broker.list':`${process.env.BROKER_LIST}`,
  'retry.backoff.ms': 200,
  // 'client.id': 'kafka',
  // 'compression.codec': 'gzip',
  'message.send.max.retries': 1,
  'socket.keepalive.enable': true,
  'queue.buffering.max.messages': 100000,
  'queue.buffering.max.ms': 1000,
  'batch.num.messages': 1000000,
  'dr_msg_cb': true,
  'event_cb': true,
  'request.required.acks': 1,
  'debug': 'broker,topic,msg',
  'fetch.wait.max.ms': 10000,
  'queuing.strategy': 'fifo', //fifo, lifo
  'enable.idempotence': true,
  'message.send.max.retries': 20
});

producer.connect();
producer.on('ready', () => {
  console.log(`😀 ready!!!`);
  console.log(`---------`);
});
producer.on('event.error', (err) => {
  console.error('😱 Log event.error');
  console.error(err);
  console.log(`---------`);
})
producer.on('event', (event) => {
  console.log('🤩 Log event');
  console.log(event);
  console.log(`---------`);
})
producer.on('data', (data) => {
  console.log('🤩 Data event');
  console.log(data);
  console.log(`---------`);
})
producer.on('delivery-report', (err, report) => { //'dr_cb': true or 'dr_msg_cb': true
  if(err){
    console.error('😱 Error delivery-report');
    console.error(err);
  }else{
    console.log(`🤩 delivery-report`);
    console.log(`Message delivered to ${report.topic}:${report.partition} at offset ${report.offset}`);
  }
});
producer.setPollInterval(100);

function createObjectForProduce(msg){
  const words = randomstring.generate(7);
  const obj = {
    node_uniqueue: uniqueue,
    message: `${words} - msg`
  }
  return obj
}

if(waitConsumerResponse ){
  consumer
    .on('ready' ,()=>{
        consumer.subscribe([`${uniqueue}`])
        consumer.consume();
        console.log(`😀 consumer: ${uniqueue} ready...`)
    })
    .on('data',(data)=>{
      console.log(`🤩 consumer data ${data}`);
      console.log(data.value.toString());
      console.log(`---------`);
    })
}

function producerSend(key, msg){
  const strObjProduce = JSON.stringify(createObjectForProduce(msg))
  producer.produce(
    `${process.env.TOPIC}`, //topic
    null, //null is default
    Buffer.from(strObjProduce), //message
    `${key}`, //keyed messages
    Date.now(),
  )
}

app.post('/:key/:msg', function (req, res) {
  try{
    producerSend(req.params.key, req.params.msg)
    console.log(`😀 consumer: sending data...`)
    if(waitConsumerResponse ){
      //responseConsumer
    }else{
      res.send('OK!')
    }
  }catch(err){
    console.error('😱 A problem occurred when sending our message');
    console.error(err);
    res.statusCode(500)
  }
})
app.listen(3000)