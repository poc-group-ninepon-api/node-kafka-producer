const kafka =  require("kafka-node")
let Producer = kafka.Producer
let KeyedMessage = kafka.KeyedMessage
const client = new kafka.KafkaClient({kafkaHost: 'svms-sunvending.com:9092'})

const producer = new Producer(client);
let km = new KeyedMessage('key', 'message');
let payloads = [
    { topic: 'topic1', messages: 'hi', partition: 0 },
    { topic: 'topic2', messages: ['hello', 'world', km] }
];

producer.on('ready', () => {
    producer.send(payloads, (err, data) => {
        if(err){
            console.log(`err`);
            console.log(err);
        }
        console.log(data);
    });
});

producer.on('error',(err) => { 
    console.log(err); 
})